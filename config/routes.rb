Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  get '/thanks' => 'pages#thanks'
  get '/signups' => 'signups#new'
  resources :signups
  # Defines the root path route ("/")
  # root "articles#index"
end
